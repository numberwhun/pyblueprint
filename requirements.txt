virtualenvwrapper==4.7.2
autoenv==1.0.0
bitbucket-cli==0.5.1
github3.py==0.9.6
pew==0.1.26
sh==1.12.9
python-gitlab==1.4.0
