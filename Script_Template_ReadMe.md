This code will add the directory 'lib/python' to 
your python path.  That said, you can write what
ever modules you need for your project, and just 
put them in that directory.  Then, just import them 
into your project as you normally would.

If you create a directory under '../lib/python', 
and then create some .py files under that directory,
you would simply import as so:

    import <directory_name>

and that will include all .py files under that 
directory.  On the other hand, if you wanted to
import only selected files from that directory, you
could import them as follows:

    from <directory> import ( file1,
                              file2,
                              file3,
                            )

That would import just those specific files.  The 
space and puttin each on a new line is for 
readability. You can change the 'libdir' directory,
but you are responsible for ensuring that whatever
you specify exists. 
