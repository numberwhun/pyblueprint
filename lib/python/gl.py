#!/usr/bin/env python

# Imports
import gitlab


def do_gitlab(gitlab_token, projectNm):

    repo = {}
    repo['name'] = projectNm
    repo['gltoken'] = gitlab_token

    #Initialize the connection string to gitlab
    glab = gitlab.Gitlab('https://gitlab.com/', private_token=repo.pop('gltoken'))

    project = None
    if repo.get('name'):
        project = glab.projects.create({'name': repo.pop('name')})

    if project:
       print("Created {0} successfully.".format(project.name))


if __name__ == "__main__":
    '''
    This module is designed to create a Gitlab repository for your new project
    '''

