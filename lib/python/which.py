#!/usr/bin/env python

import os
import os.path

def which(filename):
  """This is specifically for *nix based machines.  It will find the path to the app you specify, if it exists"""
  paths = os.environ.get("PATH").split(os.pathsep)

  candidates = []
  for location in paths:
    candidate = os.path.join(location, filename)
    if os.path.isfile(candidate):
      candidates.append(candidate)
  
  if candidates:
    return candidates
  else:
  	candidates = None
  	return candidates
