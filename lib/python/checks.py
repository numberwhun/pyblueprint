#!/usr/bin/env python
'''
This module is designed to check the setup points for pyblueprint.  It checkes to ensure that the things
that are needed to be setup and installed, are.  Running the main module without checks would potentially
cause a lot of failures.  This module attempts to thwart and circumvent those potential failures.
'''

### Imports ###
import which2
import sys
import os
import subprocess


### Variable/List/Dict Definitions###
failed_tests = []
''' This should actually be in the main script.  These methods should return true or false '''

### Functions ###

def pip_check(module):
    ''' This function is used to check to see if a module exists in the pip list'''
    installed_pkgs = subprocess.check_output(['pip', 'freeze'])
    if module in installed_pkgs:
        return True
    else:
        return False


def venv_check():
    '''
    This function ensures the virtualenv module is installed
    '''
    venv_answer = pip_check('virtualenv')
    return venv_answer


def venv_wrapper_check():
    '''
    This function ensures the virtualenvwrapper module is installed.  If so, so is
    its prerequisite, virtualenv.
    '''

    venvw_answer = pip_check('virtualenvwrapper')
    return venvw_answer


def autoenv_check():
    '''
    This function ensures the autoenv module is installed.
    '''
    autoenv_answer = pip_check('autoenv')
    return autoenv_answer

def bitbucketcli_check():
    '''
    This function ensures the bitbucket-cli module is installed.
    '''
    bitbucketcli_answer = pip_check('bitbucket-cli')
    return bitbucketcli_answer

def github_check():
    '''
    This function ensures the github3.py module is installed.
    '''
    github_answer = pip_check('github3.py')
    return github_answer

def gitlab_check():
    '''
    This function ensures the python-gitlab module is installed.
    '''
    gitlab_answer = pip_check('python-gitlab')
    return gitlab_answer

def git_check():
    '''
    This function ensures that git is installed.
    '''
    git_answer = which2.which('git')
    for i in git_answer:
      if 'git' in i:
          return True
      else:
          return False

def workon_check():
    '''
    This function ensures that WORKON_HOME variable is set in the environment.
    '''
    if not "WORKON_HOME" in os.environ:
        return False
    else:
        return True

def pew_check():
    '''
    This function ensures the github3.py module is installed.
    '''
    pew_answer = pip_check('pew')
    return pew_answer

def sh_check():
    '''
    This function ensures the github3.py module is installed.
    '''
    sh_answer = pip_check('pew')
    return sh_answer


def do_checks():
    """ This function runs all the tests and gathers the results """
    venv_chk = venv_check()
    venvw_chk = venv_wrapper_check()
    autoenv_chk = autoenv_check()
    bbcli_chk = bitbucketcli_check()
    gh_chk = github_check()
    gitlab_chk = gitlab_check()
    git_chk = git_check()
    workon_chk = workon_check()
    pew_chk = pew_check()
    sh_chk = sh_check()

    if venvw_chk == False:
        print "virtualenvwrapper module is not installed. Please install before running the application"
        sys.exit()
    elif autoenv_chk == False:
        print "autoenv module is not installed.  Please install before running the application."
        sys.exit()
    elif bbcli_chk == False:
        print "bitbucket-cli module is not installed.  Please install before running the application."
        sys.exit()
    elif gh_chk == False:
        print "github3.py module is not installed.  Please install before running the application."
        sys.exit()
    elif gitlab_chk == False:
        print "python-gitlab module is not installed.  Please install before running the application."
        sys.exit()
    elif git_chk == False:
        print "git is not installed.  Please install before running the application."
        sys.exit()
    elif workon_chk == False:
        print "WORKON_HOME is not presently set.  Please check and then re-source your bashrc file."
        sys.exit()
    elif pew_chk == False:
        print "pew is not installed.  Please install before running the application."
        sys.exit()
    elif sh_chk == False:
        print "sh is not installed.  Please install before running the application."
        sys.exit()
    else:
        print "Configuration looks good. You can now setup a project."
        sys.exit()

 ## NOTE: All of the items commented out above are pip checks.  Thanks to the pip maintainers removing
 ## the ability to easily check installed modules in the virtualenv (or at all), I have decided to 
 ## remove the checking of the modules from this script.  It now only checks that git is installed and
 ## that the workon_home variable is set.  I will continue to work on this issue, but unless I find
 ## a reliable way to check that actually lists all the modules and not a subset, I will permanently 
 ## remove these at a later date.


if __name__ == '__main__':
    '''
    Run the tests to ensure pre-requisites are installed.

    Note:  For the virtualenvwrapper test, it has to be assumed that the virtualenvwrapper.sh
    script is sourced in the users .bashrc file.  If it isn't, then the workon and other useful
    functions will not be available.
    '''

    print "Starting configuration checks for PyBluePrint....."
    do_checks()


