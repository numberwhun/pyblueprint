#!/usr/bin/env python

# Imports
import os

def do_bb(bbUser, repoName):
    bbcmd = "bb create --username {0} --public --scm git --protocol ssh {1}".format(bbUser, repoName)
    os.system(bbcmd)
    print("Set your origin as follows: git remote add origin git@bitbucket.com:{0}/{1}.git".format(bbUser, repoName))


if __name__ == '__main__':
    '''
    This module is designed to create a bitbucket repository for your new project
    '''
