#!/usr/bin/env python

# Imports
from github3 import login
from getpass import getpass

def do_github(ghUser, projectNm):
    ghUserName = ghUser

    ghPasswd = getpass('Github password for {0}: '.format(ghUserName))
    gh = login(ghUserName, password=ghPasswd)

    repo = {}
    repo['name'] = projectNm

    r = None
    if repo.get('name'):
        r = gh.create_repo(repo.pop('name'))

    if r:
        print("Created {0} successfully.".format(r.name))
        print("Set yoru origin as follows: git remote add origin git@github.com:{0}/{1}.git".format(ghUserName, projectNm))



if __name__ == '__main__':
    '''
    This module is designed to create a github repository for your new project
    '''
