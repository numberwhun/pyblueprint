#!/usr/bin/env python



def usage():
    """
    Usage:  pybp.py -c
            pybp.py -n <project_name> [-g | -b] [-p project_size]\

    
        -c                 Run configuration checks (ensures necessary pip modules are installed. 
        -n <project_name>  Supply a name for the project you are setting up
        -g                 Setup a Github repository for the project
        -b                 Setup a Bitbucket repository for the project
        -l                 Setup a Gitlab repository for the project
        -p <project_size>  Setup a flask project of size s, m or l
        -s		   Setup a standalone project, no git init will be run

    If supplying a -c, no other options should be used.  -c will ensure that necessary pip modules are installed.  If not, the script will notify you of the missing module and exit.  Rerun the script with the -c after each missing module. 

    if -s is specified, no git init will be done.  (This is for when you want to check out code from an existing repository)
    Only one of -g or -b can be specified, per project.
    Currently only Flask projects supported.
    """
    print usage.__doc__





if __name__ == '__main__':
    usage()
