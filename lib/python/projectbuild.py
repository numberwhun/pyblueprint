#!/usr/bin/env python

# Imports
import virtualenv
import pip
import os
import shutil
import distutils
import pew
from sh import git

# Functions
def do_build(projName, projBasePath, gitInit):
    fullProjPath = os.path.join(projBasePath, projName)
    libpythonPath = fullProjPath + "/lib/python"
    gitState = gitInit

    # Check if project base exists, and create it if not
    if not os.path.exists(projBasePath):
        # distutils.dir_util.mkpath(projBasePath)
        os.makedirs(projBasePath)

    # Ensure project directory exists
    if not os.path.exists(fullProjPath):
      # distutils.dir_util.mkpath(fullProjPath)
        os.makedirs(fullProjPath)
    # Create the virtual environment
    pew.pew.mkvirtualenv(projName)

    # Write out the .env file so autoenv will work
    envFile = fullProjPath + "/.env"
    line1 = "echo '" + projName + " environment ACTIVE'"
    line2 = "workon " + projName

    if gitState == "Y":
      with open(envFile, 'a') as envfile:
          envfile.write(line1 + '\n')
          envfile.write(line2 + '\n')

      # Check if lib/python exists in project, and create it if not
      if not os.path.exists(libpythonPath):
          # distutils.dir_util.mkpath(libpythonPath)
          os.makedirs(libpythonPath)

      # print "Full Project Path is: " + fullProjPath
      scriptExtension = '.py'
      scriptFile = os.path.join(fullProjPath, projName) + scriptExtension
      templateFile = "./py_script_template.py"

      # Create an empty README.md file
      # Using the open in this manner allows for simple creation
      # without truncating in case it already exists.
      readmeFile = fullProjPath + "/README.md"
      open(readmeFile, 'a').close()

      # Create an empty TODO file
      todoFile = fullProjPath + "/TODO"
      open(todoFile, 'a').close()

      # Create an empty __init__.py file
      initpyFile = fullProjPath + "/__init__.py"
      open(initpyFile, 'a').close()

      # Put script template into place
      if not os.path.exists(scriptFile):
          shutil.copy(templateFile, scriptFile)

      # Initialize the git repository, if needed
      if gitState == "Y":
          git("init", fullProjPath)

    else:
      print ""
      print "Your project was created in an empty state at your request."
      print "After your clone your repo into it, please be sure to create "
      print "a .env file in the project directory with the following lines:"
      print ""
      print line1
      print line2


if __name__ == '__main__':
    """ help text """
