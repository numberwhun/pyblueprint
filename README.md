# PyBlueprint #

The purpose of PyBlueprint is to provide a means by which to setup a new project framework for your next Python project.  

Initially pybp.py has support for:

* setup checks to ensure all the necessary components are installed and setup
* creating a project directory with a virtual environment for your project
* initialize a git repository for you to commit your code to
* creation of either a github or bitbucket repository for your project (pre-existing account required)

## Usage ##
```
#!python


  	pybp.py -c
	pybp.py -n <project_name> [-g | -b] [-p project_size]
    
    -c                 Run configuration checks (ensures necessary pip modules are installed. 
    -n <project_name>  Supply a name for the project you are setting up
    -g                 Setup a github repository for the project
    -b                 Setup a bitbucket repository for the project
    -p <project_size>  Setup a flask project of size s, m or l (TBD Feature)
    -s                 Setup a standalone project, not git init will be run.
```


If supplying a -c, no other options should be used.  -c will ensure that necessary pip modules are installed.  If not, the script will notify you of the missing module and exit.  Rerun the script with the -c after each missing module.

Only one of -g or -b can be specified, per project.  

If -s is specified, no git init will be done.  This is useful for when you want to create a virtual environment for a project, but are planning on checking out an already existing project into the virtual environment.  By default, the application sets up a git repository, so you will need to specify the -s if you want to skip that portion.

The project size template is based on Flask.  There are plans to have functionality to setup for a Django project as well.

**Minimum python version:  2.7.x**

# Installation #
First, you will want to clone the repository to your local machine.  You will need to install some requirements to get it to work, so a virtualenv is recommended, but not required.  Its up to you.

Next, you will need to install the required python modules that the script requires.  You can check what you need by running 'pydb.py -c'.  That runs the checks to see what is needed.  The recommendation though, is to run the following first:

	pip install -r ./requirements.txt

Run that from the main project directory and it will install the required modules.  Please know that this project was built with python 2.7.12+ and has not yet been tested with Python 3.  This will not work on versions less than 2.7 due to modules like pew not being available.

There is a pybp.conf file in the main directory that you will need to edit.  In this file, you will need to edit:
  * project_base_path:  This is the base path that the project will use when setting up the new python project.  
  * github_login & bitbucket_login:  These are the user names for your account in the respective sites.  You will be prompted for a password when running the script.