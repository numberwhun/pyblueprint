#!/usr/bin/env python

# Initial Imports
import os
import sys
import json

# Library Path Modification
libDir  = "lib/python"
prog    = os.path.abspath(__file__)
cwd     = os.path.dirname(prog)
sys.path.append(os.path.join(cwd,
                             libDir)
                )

# Imports from app
import checks
import argparse


# Arg parsing code
#parser = argparse.ArgumentParser()
#parser.add_argument('name', help='The string to search for(can be a partial name)')
#parser.add_argument('--option1', nargs='?', const='', default='', help='help text')
#parser.add_argument('--option2', help='help text', nargs='?', const='', default='')
#args = parser.parse_args()


# Read config file values
with open('pybp.conf', 'r') as f:
    config_data = json.load(f)


# Variables
proj_base_path = config_data['project_base_path']
gh_uname = config_data['github_login']
bb_uname = config_data['bitbucket_login']
gl_token = config_data["gitlab_token"]


# Functions
def do_checks():
    """ This function runs all the tests and gathers the results """
    venv_chk = checks.venv_check()
    venvw_chk = checks.venv_wrapper_check()
    autoenv_chk = checks.autoenv_check()
    bbcli_chk = checks.bitbucketcli_check()
    gh_chk = checks.github_check()
    git_chk = checks.git_check
    checks_list = ['venv_chk', 'venvw_chk', 'autoenv_chk', 'bbcli_chk', 'gh_chk', 'git_chk']
    failed_checks = [check for check in checks_list if False in check]

    print "The checks have been completed and have the following results:"
    if failed_checks:
        print "The checks have found the following: "
        for failed in failed_checks:
            print failed + " doesn't seem to be installed"
        print 'If these installation issues are not resolved, the application '


# Additional Imports
import argparse
import checks
import projectbuild
import usage
import json
import gh
import bb
import gl


def main():
    # Arg parsing code
    parser = argparse.ArgumentParser()
    parser.add_argument('-c', action="store_true", default=False, help='Run configuration and setup check')
    parser.add_argument('-n', '--name', action="store", dest="project_name", help='The name for the projec.  It will be used for the directory')
    parser.add_argument('-g', action="store_true", help='Setup a github project', default='False')
    parser.add_argument('-b', action="store_true", help='Setup a bitbucket project', default='False')
    parser.add_argument('-l', action="store_true", help='Setup a Gitlab project', default='False')
    parser.add_argument('-p', action="store", dest="project_size", help='Setup a project template.  Sizes available are s(mall), m(edium) and l(arge)')
    parser.add_argument('-s', action="store_true", help='Skip initializing a git repository, creates an empty project directory', default='False')
    args = parser.parse_args()
    # print args
    
    # Read config file values
    # with open('pybp.conf', 'r') as f:
    #     config_data = json.load(f)
    
    
    # Variables
    proj_base_path = config_data['project_base_path']
    gh_uname = config_data['github_login']
    bb_uname = config_data['bitbucket_login']
    gl_token = config_data["gitlab_token"]

    if len(sys.argv) <= 1:
        print "ERROR: You have not supplied any options to the script..."
        usage.usage()
        sys.exit()
    
    if args.c == True:
        checks.do_checks()
    else:
        if args.project_name == None:
            print "ERROR: You need to supply a project name"
            sys.exit()
        else:
            projectName = args.project_name
            if args.s == True:
                gitinit = "N"
 	    else:
		gitinit = "Y"
            projectbuild.do_build(projectName, proj_base_path, gitinit)

            if args.g == True:
                gh.do_github(gh_uname, projectName)
  
            if args.b == True:
                bb.do_bb(gh_uname, projectName)

            if args.l == True:
                gl.do_gitlab(gl_token, projectName)
  
if __name__ == '__main__':
    """ help text """
    # do_checks

    sys.exit(main())    

