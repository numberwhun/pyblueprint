#!/usr/bin/env python

### Imports ###
import os
import sys


### Python Path Modification For Local lib/python Directory ###
libDir  = "lib/python"
prog    = os.path.abspath(__file__)
cwd     = os.path.dirname(prog)
baseDir = os.path.dirname(cwd)
sys.path.append(os.path.join(baseDir,
                             libDir)
                )

### Arg parsing code ###
parser = argparse.ArgumentParser()
parser.add_argument('name', help='The string to search for(can be a partial name)')
parser.add_argument('--option1', nargs='?', const='', default='', help='help text')
parser.add_argument('--option2', help='help text', nargs='?', const='', default='')
args = parser.parse_args()


### Read config file values
with open('file.conf', 'r') as f:
    config_data = json.load(f)


### Functions ###
def function():
    """ help text """


def function2():
    """ more help text """


if __name__ == '__main__':
    """ help text """



